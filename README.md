# human puzzle

Repertoire gitlab pour une ré-implémentation et augmentation du jeu de vulgarisation informatique "puzzle humain".

Ce dossier comprend : 



## Script principal : ##

### Input ###

Fichier de configuration `config.py` contenant :

1. Une phrase d'entrée
2. La forme de puzzle voulue
3. Bordures externes ou non
4. Autres.

### Output ###

* PDF des pièces de puzzle
* N'utilise pas les nombres nuls (96, 69, etc..)
* Métadonnées au dos :
	1. Pavage du nom du puzzle, en majuscule si on a des numéros sur les bords, en minuscule sinon ;
	2. Numéro de pièce / Nombre de pièces total (dans un ordre aléatoire, pour ne pas donne la solution) ;
* Nombre de mots par pièce environ équitable
* SVG des pièces de puzzle disponible en bidouillant le code (simplement ne pas les supprimer après génération du PDF)

### Dépendences ###

* python3
* inkscape
* pdfunite
* maybe more..

